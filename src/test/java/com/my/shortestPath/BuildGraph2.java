package com.my.shortestPath;

public class BuildGraph2 {


    public static void main(String[] args) {
        int n = 35;//35个节点
        int e = 0;//这个不重要
        MGraph graph = new MGraph(n, e, false);
        //0
        graph.addEdge(0, 1, 3.0);
        //1
        graph.addEdge(1, 2, 3.0);
        graph.addEdge(1, 19, 3.0);
        graph.addEdge(1, 27, 3.0);
        //2
        graph.addEdge(2, 3, 3.0);
        graph.addEdge(2, 8, 10.5);
        graph.addEdge(2, 14, 10.5);
        //3
        graph.addEdge(3, 4, 3.0);
        graph.addEdge(3, 21, 3.0);
        graph.addEdge(3, 29, 3.0);
        //4
        graph.addEdge(4, 5, 3.0);
        graph.addEdge(4, 23, 3.0);
        graph.addEdge(4, 31, 3.0);
        //5
        graph.addEdge(5, 6, 3.0);
        graph.addEdge(5, 11, 10.5);
        graph.addEdge(5, 17, 10.5);
        //6
        graph.addEdge(6, 25, 3.0);
        graph.addEdge(6, 33, 3.0);
        //7
        graph.addEdge(7, 8, 3.0);
        graph.addEdge(7, 20, 1.5);
        //8
        graph.addEdge(8, 9, 3.0);
        //9
        graph.addEdge(9, 10, 3.0);
        graph.addEdge(9, 22, 1.5);
        //10
        graph.addEdge(10, 11, 3.0);
        graph.addEdge(10, 24, 1.5);
        //11
        graph.addEdge(11, 12, 3.0);
        //12
        graph.addEdge(12, 26, 1.5);
        //13
        graph.addEdge(13, 14, 3.0);
        graph.addEdge(13, 28, 1.5);
        //14
        graph.addEdge(14, 15, 3.0);
        //15
        graph.addEdge(15, 16, 3.0);
        graph.addEdge(15, 30, 1.5);
        //16
        graph.addEdge(16, 17, 3.0);
        graph.addEdge(16, 32, 1.5);
        //17
        graph.addEdge(17, 18, 3.0);
        //18
        graph.addEdge(18, 34, 1.5);


        System.out.println(graph);
        ShortestPath shortestPath = new ShortestPath();
        shortestPath.dijsktra(graph.getEdges(), 0);//由于将weight扩大了10倍，最终weight需减少10倍
    }
}
