package com.my.shortestPath;

import java.util.ArrayList;
import java.util.Arrays;

public class BuildGraph {


    public static void main(String[] args) {
        MGraph graph = new MGraph(4, 5, false);
        graph.addEdge(0, 1, 2.0);
        graph.addEdge(0, 2, 4.0);
        graph.addEdge(0, 3, 1.0);

        graph.addEdge(1, 3, 3.0);
        graph.addEdge(2, 3, 2.0);
        System.out.println(graph);
        ShortestPath shortestPath = new ShortestPath();
        shortestPath.dijsktra(graph.getEdges(), 0);
    }
}
