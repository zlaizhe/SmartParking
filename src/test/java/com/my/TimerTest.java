package com.my;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {

    public static void main(String[] args) throws InterruptedException {
        Timer timer = new Timer();
        TimerTask task1 = new TimerTask() {
            @Override
            public void run() {
                System.out.println("1s到了");
            }
        };
        TimerTask task2 = new TimerTask() {
            @Override
            public void run() {
                System.out.println("2s到了");
            }
        };

        timer.schedule(task1, 1000);
        timer.schedule(task2, 2000);
        task2.cancel();
        task2.cancel();

        Thread.sleep(3000);
        timer.cancel();
    }
}
