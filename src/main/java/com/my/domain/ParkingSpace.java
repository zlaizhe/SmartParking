package com.my.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ParkingSpace {
    private Integer id;//车位编号
    private Boolean locked;//车位是否被锁
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;//车位被开启时间
    private Integer status;//车位状态：0 空闲， 1，占用 ，2，被预定 3. 被管理员锁定

    public ParkingSpace() {
    }

    public ParkingSpace(Integer id, Boolean locked, Date startTime, Integer status) {
        this.id = id;
        this.locked = locked;
        this.startTime = startTime;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
