package com.my.domain;

//用于Controller响应JSON的实体类
public class Result {

    private boolean flag;
    private String msg;
    private Object res;

    public static Result newErrResult() {
        return newErrResult(null);
    }

    public static Result newErrResult(String msg) {
        return new Result(false, msg, null);
    }

    public static Result newResult(String msg, Object res) {
        return new Result(true, msg, res);
    }

    public static Result newResult(Object res) {
        return newResult("success", res);
    }

    public Result() {
    }

    public Result(boolean flag, String msg, Object res) {
        this.flag = flag;
        this.msg = msg;
        this.res = res;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getRes() {
        return res;
    }

    public void setRes(Object res) {
        this.res = res;
    }
}
