package com.my.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
    private Integer id;
    private String username;
    @JsonIgnore
    private String password;
    private Integer status; //0.普通用户 1.超级管理员

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
