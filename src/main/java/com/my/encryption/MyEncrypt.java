package com.my.encryption;

public class MyEncrypt {//自定义加密方法
    //HH:mm:ss#id
    private final static int[] offset = {1, -2, 3, -4, 5, -6, 7, -8, 9, -10, 11};

    public static String encode(String str) {
        if (str.length() > offset.length) {
            return null;
        }
        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (chars[i] + offset[i]);
        }
        return new String(chars);
    }

    public static String decode(String str) {
        if (str.length() > offset.length) {
            return null;
        }
        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (chars[i] - offset[i]);
        }
        return new String(chars);
    }

    public static void main(String[] args) {
        String str = "23:40:21#12";
        String encode = encode(str);
        String decode = decode(encode);
        System.out.println(encode);
        System.out.println(decode);

    }
}
