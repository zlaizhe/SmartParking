package com.my.config;

import com.my.domain.Vertex;
import com.my.shortestPath.MGraph;
import com.my.shortestPath.ShortestPath;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ShortestPathConfig {

    @Bean(name = "shortestPathMap")
    public Map<Integer, List<Integer>> createShortestPathMap() {
        int n = 35;//35个节点
        int e = 0;//这个不重要
        MGraph graph = new MGraph(n, e, false);
        //0
        graph.addEdge(0, 1, 3.0);
        //1
        graph.addEdge(1, 2, 3.0);
        graph.addEdge(1, 19, 3.0);
        graph.addEdge(1, 27, 3.0);
        //2
        graph.addEdge(2, 3, 3.0);
        graph.addEdge(2, 8, 10.5);
        graph.addEdge(2, 14, 10.5);
        //3
        graph.addEdge(3, 4, 3.0);
        graph.addEdge(3, 21, 3.0);
        graph.addEdge(3, 29, 3.0);
        //4
        graph.addEdge(4, 5, 3.0);
        graph.addEdge(4, 23, 3.0);
        graph.addEdge(4, 31, 3.0);
        //5
        graph.addEdge(5, 6, 3.0);
        graph.addEdge(5, 11, 10.5);
        graph.addEdge(5, 17, 10.5);
        //6
        graph.addEdge(6, 25, 3.0);
        graph.addEdge(6, 33, 3.0);
        //7
        graph.addEdge(7, 8, 3.0);
        graph.addEdge(7, 20, 1.5);
        //8
        graph.addEdge(8, 9, 3.0);
        //9
        graph.addEdge(9, 10, 3.0);
        graph.addEdge(9, 22, 1.5);
        //10
        graph.addEdge(10, 11, 3.0);
        graph.addEdge(10, 24, 1.5);
        //11
        graph.addEdge(11, 12, 3.0);
        //12
        graph.addEdge(12, 26, 1.5);
        //13
        graph.addEdge(13, 14, 3.0);
        graph.addEdge(13, 28, 1.5);
        //14
        graph.addEdge(14, 15, 3.0);
        //15
        graph.addEdge(15, 16, 3.0);
        graph.addEdge(15, 30, 1.5);
        //16
        graph.addEdge(16, 17, 3.0);
        graph.addEdge(16, 32, 1.5);
        //17
        graph.addEdge(17, 18, 3.0);
        //18
        graph.addEdge(18, 34, 1.5);


        //System.out.println(graph);
        //执行dijsktra算法
        ShortestPath shortestPath = new ShortestPath();
        shortestPath.dijsktra(graph.getEdges(), 0);//由于将weight扩大了10倍，最终weight需减少10倍

        //统计0到各个节点的最短路径
        String[] paths = shortestPath.getPath();
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (String path : paths) {
            String[] strings = path.split("->");
            int tar = Integer.parseInt(strings[strings.length - 1]);
            map.put(tar, new ArrayList<>());
            for (String string : strings) {
                int v = Integer.parseInt(string);
                map.get(tar).add(v);
            }
        }
        //System.out.println(map);
        return map;
    }

    @Bean(name = "vertexMap")
    public Map<Integer, Vertex> createVertexMap() {
        Map<Integer, Vertex> map = new HashMap<>();
        //0
        map.put(0, new Vertex(478, 450));
        //1-6
        map.put(1, new Vertex(478, 384));
        map.put(2, new Vertex(478, 318));
        map.put(3, new Vertex(478, 264));
        map.put(4, new Vertex(478, 192));
        map.put(5, new Vertex(478, 130));
        map.put(6, new Vertex(478, 70));

        //7-12
        map.put(7, new Vertex(218, 384));
        map.put(8, new Vertex(218, 318));
        map.put(9, new Vertex(218, 264));
        map.put(10, new Vertex(218, 192));
        map.put(11, new Vertex(218, 130));
        map.put(12, new Vertex(218, 70));

        //13-18
        map.put(13, new Vertex(728, 384));
        map.put(14, new Vertex(728, 318));
        map.put(15, new Vertex(728, 264));
        map.put(16, new Vertex(728, 192));
        map.put(17, new Vertex(728, 130));
        map.put(18, new Vertex(728, 70));

        //以下为车位节点，节点编号减去18就是车位编号
        //20 22 24 26
        map.put(20, new Vertex(138, 384));
        map.put(22, new Vertex(138, 264));
        map.put(24, new Vertex(138, 192));
        map.put(26, new Vertex(138, 70));

        //19 21 23 25
        map.put(19, new Vertex(338, 384));
        map.put(21, new Vertex(338, 264));
        map.put(23, new Vertex(338, 192));
        map.put(25, new Vertex(338, 70));

        //27 29 31 33
        map.put(27, new Vertex(618, 384));
        map.put(29, new Vertex(618, 264));
        map.put(31, new Vertex(618, 192));
        map.put(33, new Vertex(618, 70));
        //28 30 32 34
        map.put(28, new Vertex(828, 384));
        map.put(30, new Vertex(828, 264));
        map.put(32, new Vertex(828, 192));
        map.put(34, new Vertex(828, 70));


        return map;
    }

}
