package com.my.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.my.domain.ParkingSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class GlobalConfig {

    @Bean(name = "timer")
    public Timer createTimer() {
        Timer timer = new Timer();
        return timer;
    }

    @Bean(name = "timerTaskMap")
    public Map<Integer, TimerTask> createTaskMap() {//车位预定的定时任务
        Map<Integer, TimerTask> timerTaskMap = new ConcurrentHashMap<>();
        return timerTaskMap;
    }

    @Bean(name = "timerTask2Map")
    public Map<Integer, TimerTask> createTask2Map() {//车位预定的定时任务
        Map<Integer, TimerTask> timerTaskMap = new ConcurrentHashMap<>();
        return timerTaskMap;
    }

    @Bean(name = "sessionMap")
    public Map<Integer, HttpSession> createSessionMap() {
        Map<Integer, HttpSession> sessionMap = new ConcurrentHashMap<>();
        return sessionMap;
    }

    //@Bean(name = "objectMapper")
//    public ObjectMapper createObjectMapper() {//jackson自动配置已提供ObjectMapper的bean，无需配置
//        ObjectMapper objectMapper = new ObjectMapper();
//        return objectMapper;
//    }

}
