package com.my.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.my.domain.ParkingSpace;
import com.my.domain.Result;
import com.my.domain.User;
import com.my.encryption.Md5Util;
import com.my.mapper.ParkingSpaceMapper;
import com.my.mapper.UserMapper;
import com.my.service.ParkingSpaceService;
import com.my.service.UserService;
import com.my.websocket.WebSocketClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

@CrossOrigin//让这个控制器支持跨域请求
@Controller
@RequestMapping("/admin")//超级管理员操作相关
public class AdminController {

    public static final String LOGIN_USER = "loginUser";

    @Autowired
    private ParkingSpaceService parkingSpaceService;

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/login", method = RequestMethod.POST, params = {"username", "password"})
    @ResponseBody
    public Result login(User loginUser, HttpSession session) throws Exception {
        loginUser.setPassword(Md5Util.encodeByMd5(loginUser.getPassword()));//对密码MD5加密
        User user = userService.login(loginUser);
        if (user == null) {
            return Result.newErrResult("用户名或密码错误");
        }
        session.setAttribute(LOGIN_USER, user);
        return Result.newResult("登录成功", user);
    }

    @RequestMapping(path = "/checkLogin", method = RequestMethod.POST)
    @ResponseBody
    public Result checkLogin(HttpSession session) {
        User user = (User) session.getAttribute(LOGIN_USER);
        if (user == null) {
            return Result.newErrResult("请登录");
        }
        return Result.newResult("已登录", user);
    }

    @RequestMapping(path = "/exit", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result exit(HttpSession session) {
        User user = (User) session.getAttribute(LOGIN_USER);
        if (user == null) {
            return Result.newErrResult("您尚未登陆");
        }
        session.removeAttribute(LOGIN_USER);
        return Result.newResult("退出登录成功", null);
    }


    @RequestMapping(path = "/lock", method = RequestMethod.POST, params = {"id"})
    @ResponseBody
    public Result lock(Integer id) {
        ParkingSpace parkingSpace = id > 0 ? parkingSpaceService.findById(id) : null;
        if (parkingSpace == null) {
            return Result.newErrResult("该车位不存在");
        }
        if (parkingSpace.getLocked()) {//目前设定管理员只能锁定空闲的车位
            return Result.newErrResult("该车位已锁定，状态是：" + parkingSpace.getStatus());
        }

        Result result = parkingSpaceService.lock(id);
        result.setMsg("锁定" + id + "号车位成功");
        return result;
    }

    @RequestMapping(path = "/unlock", method = RequestMethod.POST, params = {"id"})
    @ResponseBody
    public Result unlock(Integer id) {
        ParkingSpace parkingSpace = id > 0 ? parkingSpaceService.findById(id) : null;
        if (parkingSpace == null) {
            return Result.newErrResult("该车位不存在");
        }
        if (!parkingSpace.getLocked()) {
            return Result.newErrResult("该车位已经空闲，状态是：" + parkingSpace.getStatus());
        }

        Result result = parkingSpaceService.unlock(id);
        result.setMsg("解锁" + id + "号车位成功");
        return result;
    }

    @RequestMapping(path = "/lockAll", method = RequestMethod.POST)
    @ResponseBody
    public Result lockAll() {
        List<ParkingSpace> parkingSpaceList = parkingSpaceService.findAll();
        for (ParkingSpace parkingSpace : parkingSpaceList) {
            if (!parkingSpace.getLocked()) {
                parkingSpaceService.lock(parkingSpace.getId());
            }
        }
        return Result.newResult("锁定所有车位完成", null);
    }

    @RequestMapping(path = "/unlockAll", method = RequestMethod.POST)
    @ResponseBody
    public Result unlockAll() {
        List<ParkingSpace> parkingSpaceList = parkingSpaceService.findAll();
        for (ParkingSpace parkingSpace : parkingSpaceList) {
            if (parkingSpace.getLocked()) {
               parkingSpaceService.unlock(parkingSpace.getId());
            }
        }
        return Result.newResult("所有车位解锁完成", null);
    }
}
