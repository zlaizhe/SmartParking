package com.my.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.my.domain.ParkingSpace;
import com.my.domain.Result;
import com.my.domain.Vertex;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin//让这个控制器支持跨域请求
@Controller
@RequestMapping("/park")
public class ShowPathController {

    @Resource
    private Map<Integer, List<Integer>> shortestPathMap;//从0到各个节点的最短路径，键：目的地节点编号，值：路径经过的节点编号

    @Resource
    private Map<Integer, Vertex> vertexMap;//每个节点在停车场示意图的坐标，键：节点编号，值：坐标，图片左上角为原点

    private static final int VERTEX_ID_OFFEST = 18;//车位id+18=节点id

    //预约成功在客户端显示车位引导路线
    @RequestMapping(path = "/path", method = RequestMethod.GET)
    @ResponseBody
    public Result showPath(HttpSession session) {
        ParkingSpace parkingSpace = (ParkingSpace) session.getAttribute("reserve");
        if (parkingSpace == null) {
            return Result.newErrResult("您没有预约车位或预约已过期，不能显示车位引导路线");
        }
        int vertexId = parkingSpace.getId() + VERTEX_ID_OFFEST;
        List<Integer> path = shortestPathMap.get(vertexId);//最短路径
        List<Vertex> pathVertexs = new ArrayList<>(path.size());
        for (Integer v : path) {
            Vertex vertex = vertexMap.get(v);
            pathVertexs.add(vertex);
        }
        return Result.newResult("请停入您预约的" + parkingSpace.getId() + "号车位", pathVertexs);
    }

}
