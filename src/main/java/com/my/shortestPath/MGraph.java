package com.my.shortestPath;

import java.util.Arrays;

//使用邻接矩阵表示有权无向图
public class MGraph {
    //顶点为空代表的权值
    public static final int NULL = 10000;
    //邻接矩阵
    private int[][] edges;
    private int n, e;//顶点数和边数
    private boolean directed;

    public MGraph(int n, int e, boolean directed) {
        this.edges = new int[n][n];
        this.n = n;
        this.e = e;
        this.directed = directed;
        for (int[] edge : edges) {
            Arrays.fill(edge, NULL);
        }
    }

//    private void addEdge(int a, int b, int weight) {
//        if (a == b) {
//            throw new IllegalArgumentException("a == b is illegal");
//        }
//        edges[a][b] = weight;
//        if (!directed) {
//            edges[b][a] = weight;
//        }
//    }

    //将weight扩大十倍，转成int型
    public void addEdge(int a, int b, double weight) {
        if (a == b) {
            throw new IllegalArgumentException("a == b is illegal");
        }
        edges[a][b] = (int) (weight * 10);
        if (!directed) {
            edges[b][a] = (int) (weight * 10);
        }
    }

    public int[][] getEdges() {
        return edges;
    }

    public int getN() {
        return n;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("[\n");
        for (int[] edge : edges) {
            res.append("\t[");
            for (int e : edge) {
                res.append((e == NULL ? "NULL" : e) + ", ");
            }
            res.delete(res.length() - 2, res.length());
            res.append("]\n");
        }
        res.append("]\n");
        return res.toString();
    }
}
