package com.my.mapper;

import com.my.domain.ParkingSpace;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ParkingSpaceMapper {
    @Select("select * from tb_parking_space")
    public List<ParkingSpace> findAll();//查询所有

    @Select("select * from tb_parking_space where id = #{value}")
    public ParkingSpace findById(int id);//通过id查询

    @Select("select * from tb_parking_space where locked = false")
    public List<ParkingSpace> findAllFree();//查询所有空闲的车位，未使用

    @Select("select * from tb_parking_space where locked = false limit 0,1")
    public ParkingSpace findOneFree();//查询一个空闲车位

    //插入数据，封装自增主键，未使用
    @Insert("insert into tb_parking_space values(null, #{locked}, #{startTime}, #{status})")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    public int save(ParkingSpace parkingSpace);

    //更新数据
    @Update("update tb_parking_space set locked = #{locked}," +
            " start_time = #{startTime}," +
            " status = #{status} " +
            " where id = #{id}")
    public int update(ParkingSpace parkingSpace);


}
