package com.my.mapper;

import com.my.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper {

    @Select("select * from tb_user where username = #{username} and password = #{password}")
    public User findByUsernameAndPassword(User loginUser);
}
