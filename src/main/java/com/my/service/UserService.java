package com.my.service;

import com.my.domain.User;

public interface UserService {
    public User login(User loginUser);
}
