package com.my.service;

import com.my.domain.ParkingSpace;
import com.my.domain.Result;

import java.util.List;

//暂时没有编写service层
public interface ParkingSpaceService {
    public static final int RESERVE_MINUTES = 1;//默认预定时间（分钟）

    public ParkingSpace findById(Integer id);

    public List<ParkingSpace> findAll();

    public ParkingSpace findOneFree();

    public Result parking(Integer id);//占用车位

    public int getCost(Integer id);

    public Result pay(Integer id, Integer money);

    public Result reserve(Integer id);

    public Result reserveUnlock(Integer id);

    public Result unlock(Integer id);

    public Result lock(Integer id);
}
