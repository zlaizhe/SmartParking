package com.my.service.impl;

import com.my.domain.User;
import com.my.mapper.UserMapper;
import com.my.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(User loginUser) {//输入只有username和password的user，查询完整信息的user对象
        return userMapper.findByUsernameAndPassword(loginUser);
    }
}
